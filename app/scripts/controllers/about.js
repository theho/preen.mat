'use strict';

/**
 * @ngdoc function
 * @name pmatApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the pmatApp
 */
angular.module('pmatApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
