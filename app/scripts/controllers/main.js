'use strict';

/**
 * @ngdoc function
 * @name pmatApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the pmatApp
 */
angular.module('pmatApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
